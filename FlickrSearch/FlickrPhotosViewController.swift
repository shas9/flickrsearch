import UIKit

enum FlickrConstants {
  static let reuseIdentifier = "FlickrCell"
  static let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
  static let itemsPerRow: CGFloat = 3
}

final class FlickrPhotosViewController: UICollectionViewController {
  
  // 1
  var selectedPhotos: [FlickrPhoto] = []
  let shareTextLabel = UILabel()
  
  var isSharing = false {
    didSet {
      // 1
      collectionView.allowsMultipleSelection = isSharing

      // 2
      collectionView.selectItem(at: nil, animated: true, scrollPosition: [])
      selectedPhotos.removeAll()

      guard let shareButton = navigationItem.rightBarButtonItems?.first else {
        return
      }

      // 3
      guard isSharing else {
        navigationItem.setRightBarButtonItems([shareButton], animated: true)
        return
      }

      // 4
      if largePhotoIndexPath != nil {
        largePhotoIndexPath = nil
      }

      // 5
      updateSharedPhotoCountLabel()

      // 6
      let sharingItem = UIBarButtonItem(customView: shareTextLabel)
      let items: [UIBarButtonItem] = [
        shareButton,
        sharingItem
      ]

      navigationItem.setRightBarButtonItems(items, animated: true)
    }
  }


  
  var largePhotoIndexPath: IndexPath? {
    didSet {
      // 2
      var indexPaths: [IndexPath] = []
      if let largePhotoIndexPath = largePhotoIndexPath {
        indexPaths.append(largePhotoIndexPath)
      }

      if let oldValue = oldValue {
        indexPaths.append(oldValue)
      }

      // 3
      collectionView.performBatchUpdates({
        self.collectionView.reloadItems(at: indexPaths)
      }, completion: { _ in
        // 4
        if let largePhotoIndexPath = self.largePhotoIndexPath {
          self.collectionView.scrollToItem(
            at: largePhotoIndexPath,
            at: .centeredVertically,
            animated: true)
        }
      })
    }
  }

  // MARK: - Properties
  private let reuseIdentifier = "FlickrCell"
  
  private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
  
  private var searches: [FlickrSearchResults] = []
  private let flickr = Flickr()
  private let itemsPerRow: CGFloat = 3
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.dragInteractionEnabled = true
    collectionView.dragDelegate = self
    
    collectionView.dropDelegate = self
    

  }



  @IBAction func shareButtonTapped(_ sender: UIBarButtonItem) {
    
    // 1
    guard !searches.isEmpty else {
      return
    }

    // 2
    guard !selectedPhotos.isEmpty else {
      isSharing.toggle()
      return
    }

    // 3
    guard isSharing else {
      return
    }

    // 1
    let images: [UIImage] = selectedPhotos.compactMap { photo in
      guard let thumbnail = photo.thumbnail else {
        return nil
      }

      return thumbnail
    }

    // 2
    guard !images.isEmpty else {
      return
    }

    // 3
    let shareController = UIActivityViewController(
      activityItems: images,
      applicationActivities: nil)

    // 4
    shareController.completionWithItemsHandler = { _, _, _, _ in
      self.isSharing = false
      self.selectedPhotos.removeAll()
      self.updateSharedPhotoCountLabel()
    }

    // 5
    shareController.popoverPresentationController?.barButtonItem = sender
    shareController.popoverPresentationController?.permittedArrowDirections = .any
    present(shareController, animated: true, completion: nil)


    
  }
  
}


// MARK: - Text Field Delegate
extension FlickrPhotosViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    guard let text = textField.text, !text.isEmpty
    else { return true }

    // 1
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    textField.addSubview(activityIndicator)
    activityIndicator.frame = textField.bounds
    activityIndicator.startAnimating()

    flickr.searchFlickr(for: text) { searchResults in
      DispatchQueue.main.async {
        activityIndicator.removeFromSuperview()

        switch searchResults {
        case .failure(let error) :
          // 2
          print("Error Searching: \(error)")
        case .success(let results):
          // 3
          print("Found \(results.searchResults.count) matching \(results.searchTerm)")
          self.searches.insert(results, at: 0)
          // 4
          self.collectionView?.reloadData()
        }
      }
    }

    textField.text = nil
    textField.resignFirstResponder()
    return true
  }
}

// MARK: - UICollectionViewDataSource
extension FlickrPhotosViewController {
  // 1
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return searches.count
  }
  
  // 2
  override func collectionView(_ collectionView: UICollectionView,
    numberOfItemsInSection section: Int) -> Int {
    return searches[section].searchResults.count
  }
  
  
  override func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: FlickrConstants.reuseIdentifier,
      for: indexPath) as? FlickrPhotoCell
    else {
      preconditionFailure("Invalid cell type")
    }

    let flickrPhoto = photo(for: indexPath)

    // 1
    cell.activityIndicator.stopAnimating()

    // 2
    guard indexPath == largePhotoIndexPath else {
      cell.imageView.image = flickrPhoto.thumbnail
      return cell
    }

    // 3
    cell.isSelected = true
    guard flickrPhoto.largeImage == nil else {
      cell.imageView.image = flickrPhoto.largeImage
      return cell
    }

    // 4
    cell.imageView.image = flickrPhoto.thumbnail

    // 5
    performLargeImageFetch(for: indexPath, flickrPhoto: flickrPhoto, cell: cell)

    return cell
  }

  
  override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    switch kind {
    
    case UICollectionView.elementKindSectionHeader:
        // 2
        let headerView = collectionView.dequeueReusableSupplementaryView(
          ofKind: kind,
          withReuseIdentifier: "\(FlickrPhotoHeaderView.self)",
          for: indexPath)

        // 3
        guard let typedHeaderView = headerView as? FlickrPhotoHeaderView
        else { return headerView }

        // 4
        let searchTerm = searches[indexPath.section].searchTerm
        typedHeaderView.titleLabel.text = searchTerm
        return typedHeaderView
      default:
        // 5
        assert(false, "Invalid element type")
    
    }
  }

}

// MARK: - Collection View Flow Layout Delegate
extension FlickrPhotosViewController: UICollectionViewDelegateFlowLayout {
  // 1
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    // 2
    
    if indexPath == largePhotoIndexPath {
      let flickrPhoto = photo(for: indexPath)
      var size = collectionView.bounds.size
      size.height -= (FlickrConstants.sectionInsets.top +
        FlickrConstants.sectionInsets.right)
      size.width -= (FlickrConstants.sectionInsets.left +
        FlickrConstants.sectionInsets.right)
      return flickrPhoto.sizeToFillWidth(of: size)
    }

    
    
    let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
    let availableWidth = view.frame.width - paddingSpace
    let widthPerItem = availableWidth / itemsPerRow
    
    //print("cell width = \(widthPerItem)")
    
    return CGSize(width: widthPerItem, height: widthPerItem)
  }
  
  // 3
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    insetForSectionAt section: Int
  ) -> UIEdgeInsets {
    return sectionInsets
  }
  
  // 4
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return sectionInsets.left
  }
}

// MARK:- Help

extension FlickrPhotosViewController {
  
  func photo(for indexPath: IndexPath) -> FlickrPhoto {
    return searches[indexPath.section].searchResults[indexPath.row]
  }
  
  func removePhoto(at indexPath: IndexPath) {
    
    print("Remove function called")
    searches[indexPath.section].searchResults.remove(at: indexPath.row)
  }

  func insertPhoto(_ flickrPhoto: FlickrPhoto, at indexPath: IndexPath) {
    searches[indexPath.section].searchResults.insert(
      flickrPhoto,
      at: indexPath.row)
  }

  
  func performLargeImageFetch(
    for indexPath: IndexPath,
    flickrPhoto: FlickrPhoto,
    cell: FlickrPhotoCell
  ) {
    // 1
    cell.activityIndicator.startAnimating()

    // 2
    flickrPhoto.loadLargeImage { [weak self] result in
      cell.activityIndicator.stopAnimating()

      // 3
      guard let self = self else {
        return
      }

      switch result {
      // 4
      case .success(let photo):
        if indexPath == self.largePhotoIndexPath {
          cell.imageView.image = photo.largeImage
        }
      case .failure:
        return
      }
    }
  }
  
  func updateSharedPhotoCountLabel() {
    if isSharing {
      shareTextLabel.text = "\(selectedPhotos.count) photos selected"
    } else {
      shareTextLabel.text = ""
    }

    shareTextLabel.textColor = themeColor

    UIView.animate(withDuration: 0.3) {
      self.shareTextLabel.sizeToFit()
    }
  }


}


